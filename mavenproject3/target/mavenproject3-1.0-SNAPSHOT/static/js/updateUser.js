Ext.onReady(function(){
    var simple = new Ext.FormPanel({
        labelWidth: 150,
        frame:true,
        title: 'Update User Form',
        bodyStyle:'padding:5px 5px 0',
        width: 350,
        defaultType: 'textfield',

        items: [{
                fieldLabel: 'Id entry for update',
                name: 'id'
            },{
                fieldLabel: 'New name',
                name: 'name'                
            }
        ],

        buttons: [{
            text: 'Update',
            handler: function() {
                var form = simple.getForm(); 
                form.url = 'app/updateUserResult'; 
                form.standardSubmit = true; 
                form.method = 'POST'; 
                form.submit();
                
            }
        }]
    });

    simple.render(document.body);   
});