package com.mycompany.mavenproject3.services;


import com.mycompany.mavenproject3.mappers.UserMapper;
import com.mycompany.mavenproject3.models.User;
import java.util.List;
import org.apache.ibatis.session.SqlSession;


public class UserService {
    SqlSession sqlSession;

    public SqlSession getSqlSession() {
        return sqlSession;
    }

    public void setSqlSession(SqlSession sqlSession) {
        this.sqlSession = sqlSession;
    }    
    
    public void addUser(User user) {        
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);             
        userMapper.addUser(user);            
    }
    
    public void updateUser(User user) {  
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);             
        userMapper.updateUser(user);            
    }    
    
    public void deleteUser(String userId) {     
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);             
        userMapper.deleteUser(userId);            
    }

    public List<User> getAllUsers() {  
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        return userMapper.getAllUsers();
    }
}
