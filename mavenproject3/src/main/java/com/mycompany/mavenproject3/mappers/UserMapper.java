package com.mycompany.mavenproject3.mappers;


import com.mycompany.mavenproject3.models.User;
import java.util.List;


public interface UserMapper {
    public void addUser(User user);
    public void updateUser(User user);
    public void deleteUser(String userId);    
    public List<User> getAllUsers();
}