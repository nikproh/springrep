<%@page contentType="text/html" pageEncoding="UTF-8"%>

<html>
    <head>
        <link rel="stylesheet" type="text/css" href="http://cdn.sencha.com/ext/gpl/3.4.1.1/resources/css/ext-all.css" />
        <title>Menu</title>
    </head>
    <body style="padding: 30px;">
        <b>Menu</b>
        <br><br>
        <a href="${pageContext.request.contextPath}/app/showAllUsers">List</a>
        <br>
        <a href="${pageContext.request.contextPath}/createUserForm">Create</a>
        <br>
        <a href="${pageContext.request.contextPath}/updateUserForm">Update</a>
        <br>
        <a href="${pageContext.request.contextPath}/deleteUserForm">Delete</a>
    </body>
</html>
