<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
    <title>List</title>
    <head>
        <link rel="stylesheet" type="text/css" href="http://cdn.sencha.com/ext/gpl/3.4.1.1/resources/css/ext-all.css" />
        <script type="text/javascript" src="http://cdn.sencha.com/ext/gpl/3.4.1.1/adapter/ext/ext-base.js"></script>
        <script type="text/javascript" src="http://cdn.sencha.com/ext/gpl/3.4.1.1/ext-all.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/showAllUsers.js"></script>
    </head>
    <body style="padding: 30px;">
        <script>
            var myData = [
            <c:forEach var="user" items="${list}">
                ['${user.userName}', '${user.userId}'],
            </c:forEach>
            ];
        </script>
        <a href="${pageContext.request.contextPath}"><b>Back to menu</b></a>
        <br><br>
        <div id="grid"></div>
    </body>
</html>